import pymysql
from sshtunnel import SSHTunnelForwarder
 

class SQLManager(object):
    def __init__(self):
        self.conn = None
        self.cursor = None
        self.server = SSHTunnelForwarder(
                ('lxplus.cern.ch', 22),    
                ssh_password="password",
                ssh_username="shxin",
                remote_bind_address=('dbod-hgtd-pdb.cern.ch', 5506 ))  
        self.server.start()
        self.connect()

    def connect(self):
        self.conn = pymysql.connect(host='127.0.0.1',              
                           port=self.server.local_bind_port,
                           user='admin',
                           passwd='HGTDdatabase',
                           db='HGTDPDB')
        self.cursor = self.conn.cursor(cursor=pymysql.cursors.DictCursor)
                    #self.cursor.execute("SELECT * FROM SensorInformation LIMIT 10")
                    #data = self.cursor.fetchall()
                    #print ("Database version : %s " % data)
                    #print(data)
                    #self.cursor.close()
                    #self.conn.close()
    def showversion(self):
        self.cursor.execute("SELECT * FROM SensorInformation LIMIT 10")
        data = self.cursor.fetchall()
        print(data)
        return data
    def close(self):
        self.cursor.close()
        self.conn.close()
    
    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

def main():
    db = SQLManager()
    data = db.showversion()
    print(data)
    db.close()


if __name__ == '__main__':
    main()
